$(document).ready(function() {
	var username;
	var connected;
	var COLORS = [
		'#e21400', '#91580f', '#f8a700', '#f78b00',
		'#58dc00', '#287b00', '#a8f07a', '#4ae8c4',
		'#3b88eb', '#3824aa', '#a700ff', '#d300e7'
	];
	var $login = $('section.login');
	var $game = $('section.game');
	var $usernameInput = $login.find('input');
	var $lobbyInput = $game.find('.lobby input');
	var $lobbyMessages = $game.find('.lobby .chat');
	var $teamInput = $game.find('.team input');
	var $teamMessages = $game.find('.team .chat');

	var API_URL = 'https://agile-falls-7513.herokuapp.com';
	var socket = io(API_URL);
	var team = io(API_URL + '/time');
	var pm = io(API_URL + '/pm');

	socket.on('login', function (data) {
		connected = true;

		// Display the welcome message
		// log('Welcome to Socket.IO Chat – ', {
		//   prepend: true
		// });
		// addParticipantsMessage(data);
	});

	var namespace = function(socket, inputMessages) {
		var events = {
			send: function(event) {
				var message = $(this).val();

				if( message && connected ) {
					socket.emit('new message', message);

					if( message.indexOf('/') === 0 ) {
						var userPM = message.substr(1, message.indexOf(' '));

						message = '[privada para]: ' + userPM + ': ' + message;
					}

					$(this)
						.val('')
						.trigger('addMessage', [{
							username: username,
							message: message
						}]);
				}
			},
			addMessage: function(event, data, options) {
				var $usernameDiv = $('<span class="username"/>')
					.text(data.username)
					.css('color', getUsernameColor(data.username));

				if( data.inbox ) {
					data.message = '[privada]: ' + data.message;
				}

				var $messageBodyDiv = $('<span class="message">')
					.text(data.message);

				var $messageDiv = $('<li/>')
					.data('username', data.username)
					.append($usernameDiv, $messageBodyDiv);

				inputMessages.append($messageDiv);
				inputMessages[0].scrollTop = inputMessages[0].scrollHeight;
			},
		};

		// Whenever the server emits 'new message', update the chat body
		socket.on('new message', function (data) {
			events.addMessage(null, data);
		});

		return {
			events: events
		};
	}

	var lobbyNamespace = namespace(socket, $lobbyMessages);

	$lobbyInput.on(lobbyNamespace.events);
	$teamInput.on(namespace(team, $teamMessages).events);

	function login() {
		username = $usernameInput.val().trim();

		// If the username is valid
		if(username) {
			$login.off('click').fadeOut(200);
			$game.delay(200).fadeIn(200, function() {
				$lobbyInput.focus();
			});

			// Tell the server your username
			socket.emit('add user', username);
			team.emit('add user', username);

			socket.on(username + ':inbox', function(data) {
				data.inbox = true;

				lobbyNamespace.events.addMessage(null, data);
			});
		}
	}

	function getUsernameColor (username) {
		// Compute hash code
		var hash = 7;
		for (var i = 0; i < username.length; i++) {
			hash = username.charCodeAt(i) + (hash << 5) - hash;
		}
		// Calculate color
		var index = Math.abs(hash % COLORS.length);
		return COLORS[index];
	}

	function sendMessage () {
		var message = $lobbyInput.val();

		// Prevent markup from being injected into the message
		// message = cleanInput(message);

		// if there is a non-empty message and a socket connection
		if( message && connected ) {
			$lobbyInput.val('');
			addChatMessage({
				username: username,
				message: message
			});

			// tell server to execute 'new message' and send along one parameter
			socket.emit('new message', message);
		}
	}

	function sendTeamMessage() {
		var message = $teamInput.val();

		// Prevent markup from being injected into the message
		// message = cleanInput(message);

		// if there is a non-empty message and a socket connection
		if( message && connected ) {
			$teamInput.val('');
			addChatMessage({
				username: username,
				message: message
			});

			// tell server to execute 'new message' and send along one parameter
			team.emit('new message', message);
		}
	}

	

	// Log a message
	// function log(message, options) {
	// 	var $el = $('<li>').addClass('log').text(message);
	// 	addElement($el, options);
	// }

	// function addParticipantsMessage (data) {
	// 	var message = '';
	// 	if( data.numUsers === 1 ) {
	// 		message += "there's 1 participant";
	// 	}
	// 	else {
	// 		message += "there are " + data.numUsers + " participants";
	// 	}
	// 	log(message);
	// }

	// Adds the visual chat message to the message list
	

	$login.click(function() {
		$usernameInput.focus();
	});

	$lobbyMessages.click(function() {
		$teamInput.focus();
	})

	$teamMessages.click(function() {
		$teamInput.focus();
	})

	$(window).keydown(function (event) {
		// Auto-focus the current input when a key is typed
		// if (!(event.ctrlKey || event.metaKey || event.altKey)) {
		// 	$currentInput.focus();
		// }

		// When the client hits ENTER on their keyboard
		if (event.which === 13) {
			if (username) {
				$(event.target).trigger('send');
				// socket.emit('stop typing');
				// typing = false;
			} else {
				login();
			}
		}
	});
});
