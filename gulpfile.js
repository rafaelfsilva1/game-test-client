var gulp = require('gulp');

var del = require('del');
var jshint = require('gulp-jshint');
var compass = require('gulp-compass');
var concat = require('gulp-concat');
var minifyCSS = require('gulp-minify-css');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');

var paths = {
	fonts: ['fonts/**/*.*'],
	images: ['img/**/*.*'],
	sass: 'sass/**/*.scss',
	plugins: {
		css: [],
		js: [
			'bower_components/socket.io-client/socket.io.js'
		]
	}
}

// Copy plugins
gulp.task('copy-css', function() {
	gulp.src(paths.plugins.css)
		.pipe(gulp.dest('css'))
		.pipe(concat('plugins.min.css'))
		.pipe(gulp.dest('css'))
});

gulp.task('copy-js', function() {
	gulp.src(paths.plugins.js)
		.pipe(gulp.dest('js'))
		.pipe(concat('plugins.min.js'))
		.pipe(gulp.dest('js'));
});

gulp.task('sass', function() {
	gulp.src(paths.sass)
		.pipe(compass({
			sass: 'sass',
			css: 'css',
			image: 'img',
			style: 'expanded',
			comments: false
		}))
		.pipe(gulp.dest('css'));
		// .pipe(minifyCSS())
		// .pipe(gulp.dest('build/css'))
});

// Dists
gulp.task('dist-fonts', function() {
	del('fonts/**/*.*', function(err, deletedFiles) {
		console.log(deletedFiles.length, 'files has been deleted from fonts');
	});

	gulp.src(paths.fonts)
		.pipe(gulp.dest('fonts'));
});

gulp.task('dist-img', function() {
	del('img/**/*.*', function(err, deletedFiles) {
		console.log(deletedFiles.length, 'files has been deleted from img');
	});

	gulp.src(paths.images)
		.pipe(gulp.dest('img'));
});

gulp.task('dist-css', function() {
	gulp.src(['css/plugins.min.css', 'css/main.css'])
		.pipe(gulp.dest('css'))
		.pipe(concat('dist.min.css'))
		.pipe(gulp.dest('css'));
});

gulp.task('dist-js', function() {
	gulp.src('js/main.js')
		.pipe(jshint({
			curly: true,
			forin: true
		}))
		.pipe(jshint.reporter('default'));

	gulp.src(['js/plugins.min.js', 'js/main.js'])
		.pipe(gulp.dest('js'))
		.pipe(concat('dist.min.js'))
		.pipe(gulp.dest('js'));
});

gulp.task('watch', function() {
	gulp.watch(paths.plugins.css, ['copy-css']);
	gulp.watch(paths.plugins.js, ['copy-js']);
	gulp.watch(paths.sass, ['sass']);
	gulp.watch(paths.fonts, ['dist-fonts']);
	gulp.watch(paths.images, ['dist-img']);
	gulp.watch([paths.plugins.css, 'src/css/main.css'], ['dist-css']);
	gulp.watch([paths.plugins.js, 'src/js/main.js'], ['dist-js']);
});

// Default Task
gulp.task('default', ['copy-css', 'copy-js', 'sass', 'dist-fonts', 'dist-img', 'dist-css', 'dist-js', 'watch']);
